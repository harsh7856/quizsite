﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace quizSite.Context
{
    public class BaseMongoRepository
    {
        protected static IMongoDatabase _db;

        public IMongoDatabase MongoDatabase
        {
            get { return _db; }
        }
    }
}
