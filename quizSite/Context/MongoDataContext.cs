﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using quizSite.Interface;
using quizSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace quizSite.Context
{
    public class MongoDataContext : BaseMongoRepository, IMongoDataContext
    {
        public MongoDataContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _db = client.GetDatabase(settings.Value.Database);
        }
        public IMongoCollection<T> GetCollection<T>(string collectionName = null) where T : class
        {
            return _db.GetCollection<T>(getCollectionName<T>(collectionName));
        }

        protected string getCollectionName<T>(string collectionName)
        {
            return string.IsNullOrWhiteSpace(collectionName) ? typeof(T).Name.ToLower() : collectionName;
        }

        public IQueryable<T> AsQueryable<T>() where T : class
        {
            return GetCollection<T>().AsQueryable();
        }

        public Task Insert<T>(T data, string collectionName = null) where T : class
        {  
            var insertOneAsync = GetCollection<T>(collectionName)
                            .InsertOneAsync(data);
            insertOneAsync.Wait();
            return insertOneAsync;
        }
    }
}
