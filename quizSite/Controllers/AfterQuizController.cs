﻿using Microsoft.AspNetCore.Mvc;
using quizSite.DTO;
using quizSite.Interface;
using quizSite.Models;
using System;
using System.Collections.Generic;

namespace quizSite.Controllers
{
    public class AfterQuizController : Controller
    {
        private readonly IAfterquiz _afterquizservice;
        private readonly IHome _homeservice;

        public AfterQuizController(IAfterquiz afterQuizService, IHome homeService)
        {
            _afterquizservice = afterQuizService;
            _homeservice = homeService;

        }

        public IActionResult Index(string id)
        {
            var Username = _afterquizservice.GetUsername(id.ToString());
            var model = new AfterQuizUser();
            model.OldResults = _afterquizservice.GetOldResults(id);
            model.ParentUserName = Username;
            model.ParentUserId = id.ToString();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create([Bind]AfterQuizUser afterQuizUser)
        {
            if (ModelState.IsValid)
            {
                afterQuizUser.CreatedDate = DateTime.Now;
                _afterquizservice.AddAfterQuizUser(afterQuizUser);
                return RedirectToAction("Validate", new { serialno = 1, puid = afterQuizUser.ParentUserId, cuid = afterQuizUser.Id });
            }
            return View(afterQuizUser);
        }

        public IActionResult Validate(int serialno, string puid, string cuid)
        {
            var pData = _afterquizservice.GetParentUserData(puid);
            var model = new AfterQuizQuestion();
            model.ParentUserId = puid;
            model.CurrentUserId = cuid;
            model.ParentUserName = pData.Username;
            model.question = _homeservice.GetQuestion(serialno);
            model.ParentUserAnswer = pData.Answers.Find(x => x.QuestionId == model.question.Id).OptionText;
            return View("AfterQuizParent", model);
        }


        [HttpPost]
        public bool AfterQuizAnswerSave(string cuid, string puid, string questionId, string optionText, bool isCorrectAnswer)
        {
            var answer = new DTO.Answer()
            {
                IsCorrectAnswer = isCorrectAnswer,
                OptionText = optionText,
                QuestionId = questionId
            };

            return _afterquizservice.UpdateAfterQuizUser(answer, cuid);
        }

        [HttpGet]
        public IActionResult AfterQuizFinish(string CurrentUserId)
        {
            var model = new AfterQuizUser();
            model = _afterquizservice.AfterQuizUpdateStatus(CurrentUserId);
            model.OldResults = _afterquizservice.GetOldResults(model.ParentUserId);
            return View(model);
        }
    }
}