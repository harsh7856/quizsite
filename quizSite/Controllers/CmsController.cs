﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using quizSite.Interface;
using quizSite.Models;

namespace quizSite.Controllers
{
    public class CmsController : Controller
    {
        private readonly ICms _cmsservice;

        public CmsController(ICms cmsservice)
        {
            _cmsservice = cmsservice;
        }
        public IActionResult Index()
        {
            var model = new Question();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Question question)
        {
            _cmsservice.AddQuestion(question);
            return RedirectToAction("Index");
        }
        public ActionResult showmoreresult(int number)
        {
            TempData["Number"] = number;
            return PartialView("_option");
        }
    }
}