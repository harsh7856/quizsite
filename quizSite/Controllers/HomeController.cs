﻿using Microsoft.AspNetCore.Mvc;
using quizSite.Interface;
using quizSite.Models;

namespace quizSite.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHome _homeservice;

        public HomeController(IHome homeService)
        {
            _homeservice = homeService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult AboutUs()
        {
            return View();
        }

        public IActionResult PrivacyPolicy()
        {
            return View();
        }

        public IActionResult ContactUs()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind]User user)
        {
            if (ModelState.IsValid)
            {
                _homeservice.AddUser(user);
                return RedirectToAction("Quiz", new { serialno = 1, id = user.Id });
            }
            return View(user);
        }

        public IActionResult Quiz(int serialno)
        {
            var question = _homeservice.GetQuestion(serialno);

            return View("Parent", question);
        }


        public JsonResult SaveUserAnswer(string QuestionId, string OptionText, string UserId)
        {
            var answer = new Answer();
            answer.QuestionId = QuestionId;
            answer.OptionText = OptionText;
            var result = _homeservice.AddAnswer(answer, UserId);

            return Json(result);
        }

        public string IsAnswerAvailable(string questionId, string userId)
        {
            var OptionText = _homeservice.GetAnswer(questionId, userId);
            return OptionText;
        }

        [HttpGet]
        public IActionResult Finish(string userId)
        {
            int count = _homeservice.AnswerCount(userId);
            if (count < 20)
            {
                TempData["alert"] = "Please answer all 20 questions.";
            }
            else if (count == 20)
            {
                var user = _homeservice.UpdateStatus(userId);

                var finishModel = new FinishModel();
                finishModel.Name = user.Username;
                finishModel.Url = "http://" + Request.Host + "/afterquiz/" + userId;
                return View("Finish", finishModel);
            }
            return Quiz(20);
        }
    }
}