﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static quizSite.Models.Common;

namespace quizSite.DTO
{
    public class AfterQuizUser
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        [BsonRepresentation(BsonType.String)]
        public string Id { get; set; }
        public string ParentUserId { get; set; }
        public string ParentUserName { get; set; }
        public string CurrentUserName { get; set; }
        public List<Answer> Answer { get; set; }
        public DateTime CreatedDate { get; set; }
        public Status Status { get; set; }
        public decimal Score { get; set; }
        public List<OldResult> OldResults { get; set; }
    }
    public class Answer
    { 
        public string QuestionId { get; set; }
        public string OptionText { get; set; }
        public bool IsCorrectAnswer { get; set; }
    }

    public class OldResult
    {
        public string RName { get; set; }
        public decimal RScore { get; set; }
        
    }

}
