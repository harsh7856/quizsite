﻿using quizSite.DTO;
using quizSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace quizSite.Interface
{
    public interface IAfterquiz
    {
        string GetUsername(string userId);
        AfterQuizUser AddAfterQuizUser(AfterQuizUser param);
        User GetParentUserData(string puid);
        bool UpdateAfterQuizUser(DTO.Answer answer,string cuid);
        int AfterQuizAnswerCount(string cuid);
        AfterQuizUser AfterQuizUpdateStatus(string cuid);
        List<OldResult> GetOldResults(string puid);
    }
}