﻿using quizSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace quizSite.Interface
{
    public interface IHome
    {
        User AddUser(User param);
        Question GetQuestion(int serialNo);
        bool AddAnswer(Answer answer, string userId);
        string GetAnswer(string questionId, string userId);
        int AnswerCount(string userId);
        User UpdateStatus(string userId);

    }
}
