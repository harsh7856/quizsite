﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace quizSite.Interface
{
    interface IMongoDataContext
    {
        IQueryable<T> AsQueryable<T>() where T : class;
        Task Insert<T>(T data, string collectionName = null) where T : class;
        IMongoCollection<T> GetCollection<T>(string collectionName = null) where T : class;
    }
}
