﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace quizSite.Models
{
    public class AfterQuizQuestion
    {
        public Question question { get; set; }
        public string ParentUserAnswer { get; set; }
        public string ParentUserId { get; set; }
        public string ParentUserName { get; set; }
        public string CurrentUserId { get; set; }

    }
}
