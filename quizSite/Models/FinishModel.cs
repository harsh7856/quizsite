﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace quizSite.Models
{
    public class FinishModel
    {
        public string Url { get; set; }
        public string Name { get; set; }
    }
}
