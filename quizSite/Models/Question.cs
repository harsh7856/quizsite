﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace quizSite.Models
{
    public class Question
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        [BsonRepresentation(BsonType.String)]
        public string Id { get; set; }
        public string QuestionText { get; set; }
        public string AfterQuizQuestionText { get; set; }
        public List<Option> Options { get; set; }
        public int SerialNo { get; set; }
        public Option Answer { get; set; }


        public Question() // constructor
        {
            Options = new List<Option>();
            Answer = new Option();
        }
    }

    public class Option
    {
        public string OptionText { get; set; }
        public string ImageUrl { get; set; }
    }
}
