﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static quizSite.Models.Common;

namespace quizSite.Models
{
    public class User
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        [BsonRepresentation(BsonType.String)]
        public string Id { get; set; }
        public string Username { get; set; }
        public List<Answer> Answers { get; set; }
        public Status Status { get; set; }
        public int ReferenceCount { get; set; }
    }

    public class Answer
    {
        public string QuestionId { get; set; }
        public string OptionText { get; set; }
    }
}