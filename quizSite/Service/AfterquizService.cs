﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using quizSite.Context;
using quizSite.DTO;
using quizSite.Interface;
using quizSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace quizSite.Service
{
    public class AfterquizService : IAfterquiz
    {
        private readonly IMongoDataContext _context = null;

        public AfterquizService(IOptions<Settings> settings)
        {
            _context = new MongoDataContext(settings);
        }

        public AfterQuizUser AddAfterQuizUser(AfterQuizUser param)
        {
            _context.Insert(param);
            return param;
        }

        public string GetUsername(string userId)
        {
            var user = _context.GetCollection<User>().Find(c => c.Id == userId).FirstOrDefault();
            if (user == null)
            {
                return string.Empty;
            }
            return user.Username;
        }

        public User GetParentUserData(string puid)
        {
            var filter = Builders<User>.Filter.Eq("Id", puid);
            var data = _context.GetCollection<User>().Find(filter, new FindOptions());
            return data.ToList().FirstOrDefault();
        }

        public bool UpdateAfterQuizUser(DTO.Answer answer, string cuid)
        {
            var objUser = _context.GetCollection<AfterQuizUser>().Find(x => x.Id == cuid).FirstOrDefault();
            if (objUser.Answer == null)
            {
                objUser.Answer = new List<DTO.Answer>();
            }
            var checkQuestionIdAvailable = objUser.Answer.Where(x => x.QuestionId == answer.QuestionId).FirstOrDefault();
            if (checkQuestionIdAvailable != null)
            {
                objUser.Answer.Where(x => x.QuestionId == answer.QuestionId).FirstOrDefault().OptionText = answer.OptionText;
            }
            else
            {
                objUser.Answer.Add(answer);
            }
            var filter = Builders<AfterQuizUser>.Filter.Eq(s => s.Id, cuid);
            var update = Builders<AfterQuizUser>.Update
                .Set(c => c.Answer, objUser.Answer);

            _context.GetCollection<AfterQuizUser>().UpdateOne(filter, update);
            return true;
        }

        public int AfterQuizAnswerCount(string cuid)
        {
            var user = _context.GetCollection<AfterQuizUser>().Find(x => x.Id == cuid).FirstOrDefault();
            if (user.Answer != null)
            {
                return user.Answer.Count;
            }
            return 0;
        }

        public AfterQuizUser AfterQuizUpdateStatus(string cuid)
        {
            var data = _context.GetCollection<AfterQuizUser>().Find(x => x.Id == cuid).FirstOrDefault();
            var isCorrectAnswer = data.Answer.FindAll(x => x.IsCorrectAnswer == true);
            var score = isCorrectAnswer.Count;
            data.Score = score;
            var filter = Builders<AfterQuizUser>.Filter.Eq(c => c.Id, cuid);
            var update = Builders<AfterQuizUser>.Update
                .Set(a => a.Status, Common.Status.Completed)
                .Set(a => a.Score, score);

            _context.GetCollection<AfterQuizUser>().UpdateOne(filter, update);
            return data;
        }

        public List<OldResult> GetOldResults(string puid)
        {
            var result = new List<OldResult>();
            var data = _context.GetCollection<AfterQuizUser>().Find(x => x.ParentUserId == puid).ToList();
            foreach (var item in data)
            {
                var _OldResult = new OldResult();
                _OldResult.RName = item.CurrentUserName;
                _OldResult.RScore = item.Score;
                result.Add(_OldResult);
            }
            return result;
        }
    }
}
