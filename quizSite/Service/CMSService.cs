﻿using Microsoft.Extensions.Options;
using quizSite.Context;
using quizSite.Interface;
using quizSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace quizSite.Service
{
    public class CmsService : ICms
    {
        private readonly IMongoDataContext _context = null;

        public CmsService(IOptions<Settings> settings)
        {
            _context = new MongoDataContext(settings);
        }
        public Question AddQuestion(Question param)
        {
            param.SerialNo = GetSerialNo();
            _context.Insert(param);
            return param;
        }

        public int GetSerialNo()
        {
            var list = _context.AsQueryable<Question>().ToList();
            var serialno = list.Count + 1;
            return serialno;
        }
    }
}
