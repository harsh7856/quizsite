﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using quizSite.Context;
using quizSite.Interface;
using quizSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace quizSite.Service
{
    public class HomeService : IHome
    {
        private readonly IMongoDataContext _context = null;

        public HomeService(IOptions<Settings> settings)
        {
            _context = new MongoDataContext(settings);
        }

        public User AddUser(User param)
        {
            _context.Insert(param);
            return param;
        }

        public Question GetQuestion(int serialNo)
        {
            var filter = Builders<Question>.Filter.Eq("SerialNo", serialNo);
            var data = _context.GetCollection<Question>().Find(filter, new FindOptions());
            return data.ToList().FirstOrDefault();
        }

        public bool AddAnswer(Answer answer, string userId)
        {
            // this code is just to get user 
            var user = _context.GetCollection<User>().Find(c => c.Id == userId).FirstOrDefault();
            // first time user answer will null so only need to create new object when user.Answers is null

            if (user.Answers == null)
            {
                user.Answers = new List<Answer>();
            }

            var check = user.Answers.Where(x => x.QuestionId == answer.QuestionId).FirstOrDefault(); // this will reurn true if questionid available // agar true hua means pehle se hai ye question
            if (check != null)
            {
                user.Answers.Where(x => x.QuestionId == answer.QuestionId).FirstOrDefault().OptionText = answer.OptionText;
            }
            else
            {
                user.Answers.Add(answer);
            }


            // this code is to filter for update operation
            var filter = Builders<User>.Filter.Eq(s => s.Id, userId);
            var update = Builders<User>.Update
                .Set(c => c.Answers, user.Answers);

            _context.GetCollection<User>().UpdateOne(filter, update);
            return true;
        }

        public string GetAnswer(string questionId, string userId)
        {
            var user = _context.GetCollection<User>().Find(c => c.Id == userId).FirstOrDefault();
            //var filter = Builders<Answer>.Filter.Eq(x => x.OptionText, questionId);
            if (user != null)
            {
                if (user.Answers != null)
                {
                    var ot = user.Answers.Where(x => x.QuestionId == questionId).FirstOrDefault();
                    if (ot != null)
                    {
                        return ot.OptionText.Replace(' ', '_');
                    }
                }
            }
            return null;
        }

        public int AnswerCount(string userId)
        {
            var user = _context.GetCollection<User>().Find(c => c.Id == userId).FirstOrDefault();
            if (user != null && user.Answers != null)
            {
                return user.Answers.Count;
            }
            return 0;
        }

        public User UpdateStatus(string userId)
        {
            var filter = Builders<User>.Filter.Eq(s => s.Id, userId);
            var update = Builders<User>.Update
                .Set(c => c.Status, Common.Status.Completed);

            _context.GetCollection<User>().UpdateOne(filter, update);

            return _context.GetCollection<User>().Find(c => c.Id == userId).FirstOrDefault();

        }
    }
}
