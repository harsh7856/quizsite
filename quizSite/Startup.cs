﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using quizSite.Interface;
using quizSite.Service;
using quizSite.Models;

namespace quizSite
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<ICms, CmsService>();
            services.AddTransient<IHome, HomeService>();
            services.AddTransient<IAfterquiz, AfterquizService>();
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.Configure<Settings>(options =>
            {
                options.ConnectionString = Configuration.GetSection("MongoConnection:ConnectionString").Value;
                options.Database = Configuration.GetSection("MongoConnection:Database").Value;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "AfterQuizFinish",
                    template: "afterquiz/afterquizfinish",
                    defaults: new { controller = "afterquiz", action = "afterquizfinish" }
                    );

                routes.MapRoute(
                    name: "AfterQuizAnswerSave",
                    template: "afterquiz/afterquizanswersave/",
                    defaults: new { controller = "afterquiz", action = "afterquizanswersave" }
                    );

                routes.MapRoute(
                    name: "AfterQuizValidate",
                    template: "afterquiz/validate/{serialno}/{puid}/{cuid}",
                    defaults: new { controller = "afterquiz", action = "validate" }
                    );
                routes.MapRoute(
                    name: "AfterQuizUser",
                    template: "afterquiz/create",
                    defaults: new { controller = "afterquiz", action = "create" }
                    );
                routes.MapRoute(
                    name: "AfterQuizIndex",
                    template: "afterquiz/{id}",
                    defaults: new { controller = "afterquiz", action = "index" }
                    );


                routes.MapRoute(
                    name: "loadmore",
                    template: "cms/showmoreresult/{number?}",
                     defaults: new { controller = "cms", action = "showmoreresult" });

                routes.MapRoute(
                  name: "GetUserId",
                  template: "quiz/finish",
                   defaults: new { controller = "home", action = "finish" });

                routes.MapRoute(
                    name: "GetQuizQuestion",
                    template: "quiz/{serialno?}",
                     defaults: new { controller = "home", action = "quiz" });


                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
