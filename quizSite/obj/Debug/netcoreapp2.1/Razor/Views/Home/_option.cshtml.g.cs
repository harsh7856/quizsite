#pragma checksum "D:\Projects\quizSite\quizSite\Views\Home\_option.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5820e6b16f79568b7241c6f938e6907dffa11c2a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home__option), @"mvc.1.0.view", @"/Views/Home/_option.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/_option.cshtml", typeof(AspNetCore.Views_Home__option))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Projects\quizSite\quizSite\Views\_ViewImports.cshtml"
using quizSite;

#line default
#line hidden
#line 2 "D:\Projects\quizSite\quizSite\Views\_ViewImports.cshtml"
using quizSite.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5820e6b16f79568b7241c6f938e6907dffa11c2a", @"/Views/Home/_option.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fd280df2311ebfb1670ac261d4b4a1aec26b89ba", @"/Views/_ViewImports.cshtml")]
    public class Views_Home__option : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<quizSite.Models.Option>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(31, 148, true);
            WriteLiteral("\r\n<div class=\"col-xs-6 col-md-3 useranswer\">\r\n    <div class=\"thumbnail\" style=\"cursor: pointer;\">\r\n\r\n        <img style=\"width:200px;height:130px;\"");
            EndContext();
            BeginWriteAttribute("src", " src=\"", 179, "\"", 200, 1);
#line 6 "D:\Projects\quizSite\quizSite\Views\Home\_option.cshtml"
WriteAttributeValue("", 185, Model.ImageUrl, 185, 15, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("alt", " alt=\"", 201, "\"", 224, 1);
#line 6 "D:\Projects\quizSite\quizSite\Views\Home\_option.cshtml"
WriteAttributeValue("", 207, Model.OptionText, 207, 17, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(225, 35, true);
            WriteLiteral(">\r\n\r\n        <div>\r\n            <h2");
            EndContext();
            BeginWriteAttribute("id", " id=\"", 260, "\"", 299, 1);
#line 9 "D:\Projects\quizSite\quizSite\Views\Home\_option.cshtml"
WriteAttributeValue("", 265, Model.OptionText.Replace(' ','_'), 265, 34, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(300, 56, true);
            WriteLiteral(" align=\"center\" style=\"color:black;font-size: x-large;\">");
            EndContext();
            BeginContext(357, 16, false);
#line 9 "D:\Projects\quizSite\quizSite\Views\Home\_option.cshtml"
                                                                                                          Write(Model.OptionText);

#line default
#line hidden
            EndContext();
            BeginContext(373, 55, true);
            WriteLiteral("</h2>\r\n            <input type=\"hidden\" id=\"OptionText\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 428, "\"", 453, 1);
#line 10 "D:\Projects\quizSite\quizSite\Views\Home\_option.cshtml"
WriteAttributeValue("", 436, Model.OptionText, 436, 17, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(454, 43, true);
            WriteLiteral(" />\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<quizSite.Models.Option> Html { get; private set; }
    }
}
#pragma warning restore 1591
