#pragma checksum "D:\Projects\quizSite\quizSite\Views\Home\Finish.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1c808b12f8248bb37b02a619081d338a8d7e0bf5"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Finish), @"mvc.1.0.view", @"/Views/Home/Finish.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Finish.cshtml", typeof(AspNetCore.Views_Home_Finish))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Projects\quizSite\quizSite\Views\_ViewImports.cshtml"
using quizSite;

#line default
#line hidden
#line 2 "D:\Projects\quizSite\quizSite\Views\_ViewImports.cshtml"
using quizSite.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1c808b12f8248bb37b02a619081d338a8d7e0bf5", @"/Views/Home/Finish.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fd280df2311ebfb1670ac261d4b4a1aec26b89ba", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Finish : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<quizSite.Models.FinishModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "text", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-control form-control-lg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("finishUrl"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(36, 244, true);
            WriteLiteral("\r\n<div class=\"container\">\r\n    <div class=\"btn-danger\"><h1 class=\"text-center\">Your Challenge is Ready</h1></div>\r\n    <div class=\"col-lg-12\">\r\n        <h3 style=\"color:grey;\" class=\"text-center\">Share this link with your friends</h3>\r\n        ");
            EndContext();
            BeginContext(280, 87, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "d703bf54f587419ca8cd1a51f9c369c8", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
#line 7 "D:\Projects\quizSite\quizSite\Views\Home\Finish.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Url);

#line default
#line hidden
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(367, 212, true);
            WriteLiteral("\r\n        <div align=\"center\">\r\n            <button type=\"button\" class=\"btn btn-danger\" onclick=\"copyText();\">Copy this link.</button>\r\n        </div>\r\n        <br />\r\n    </div>\r\n    <div class=\"text-center\">\r\n");
            EndContext();
            BeginContext(669, 10, true);
            WriteLiteral("        <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 679, "\"", 859, 23);
            WriteAttributeValue("", 686, "https://api.whatsapp.com/send?text=🤗", 686, 37, true);
#line 15 "D:\Projects\quizSite\quizSite\Views\Home\Finish.cshtml"
WriteAttributeValue(" ", 723, Model.Name, 724, 11, false);

#line default
#line hidden
            WriteAttributeValue(" ", 735, "has", 736, 4, true);
            WriteAttributeValue(" ", 739, "sent", 740, 5, true);
            WriteAttributeValue(" ", 744, "you", 745, 4, true);
            WriteAttributeValue(" ", 748, "Super", 749, 6, true);
            WriteAttributeValue(" ", 754, "Dare", 755, 5, true);
            WriteAttributeValue(" ", 759, "of", 760, 3, true);
            WriteAttributeValue(" ", 762, "2020", 763, 5, true);
            WriteAttributeValue(" ", 767, "👸🤴.Let\'s", 768, 11, true);
            WriteAttributeValue(" ", 778, "test", 779, 5, true);
            WriteAttributeValue(" ", 783, "how", 784, 4, true);
            WriteAttributeValue(" ", 787, "much", 788, 5, true);
            WriteAttributeValue(" ", 792, "we", 793, 3, true);
            WriteAttributeValue(" ", 795, "know", 796, 5, true);
            WriteAttributeValue(" ", 800, "each", 801, 5, true);
            WriteAttributeValue(" ", 805, "other!!", 806, 8, true);
            WriteAttributeValue(" ", 813, "Share", 814, 6, true);
            WriteAttributeValue(" ", 819, "your", 820, 5, true);
            WriteAttributeValue(" ", 824, "friends", 825, 8, true);
            WriteAttributeValue(" ", 832, "also", 833, 5, true);
            WriteAttributeValue(" ", 837, "👇👇👇👇", 838, 9, true);
#line 15 "D:\Projects\quizSite\quizSite\Views\Home\Finish.cshtml"
WriteAttributeValue(" \r\n", 846, Model.Url, 849, 10, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(860, 719, true);
            WriteLiteral(@" class=""fa fa-whatsapp""></a>
    </div>
</div>
<style>
    .fa {
        padding: 17px;
        font-size: 45px;
        width: 76px;
        text-align: center;
        text-decoration: none;
        border-radius: 50%;
    }

        .fa:hover {
            opacity: 0.7;
        }

    .fa-facebook {
        background: #3B5998;
        color: white;
    }

    .fa-whatsapp {
        background: #25d366;
        color: white;
    }
</style>

<script>
    function copyText() {
        var copyTextField = document.getElementById(""finishUrl"");
        copyTextField.select();
        copyTextField.setSelectionRange(0, 99999)
        document.execCommand(""copy"");
    }
</script>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<quizSite.Models.FinishModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
